let homeScore = 0;
let guestScore = 0;
let period = 1;
let timeLeft = 0;

let leader = "none";

let homeScoreEl = document.getElementById("home-score");
let guestScoreEl = document.getElementById("guest-score");
let timeEl = document.getElementById("time");

let myTimer = -1;

function tick() {
  timeLeft--;
  if (timeLeft <= 0) {
    clearInterval(myTimer);
    myTimer = -1;
    period += 1;
    return;
  }
  updateTime();
}

window.startTimer = function startTimer() {
  if (myTimer < 0) {
    myTimer = setInterval(tick, 1000);
  }
};

window.stopTimer = function stopTimer() {
  if (myTimer !== undefined) {
    clearInterval(myTimer);
    myTimer = -1;
  }
};

function updateTime() {
  let minutes = Math.floor(timeLeft / 60);
  let timeText = "";
  if (minutes < 10) {
    timeText = "0" + minutes;
  } else {
    timeText = String(minutes);
  }
  let seconds = timeLeft % 60;
  if (seconds < 10) {
    timeText += ":0" + seconds;
  } else {
    timeText += ":" + seconds;
  }
  timeEl.textContent = timeText;
}

function updateScores() {
  homeScoreEl.textContent = homeScore;
  guestScoreEl.textContent = guestScore;

  if (leader !== "home" && homeScore > guestScore) {
    // Home takes the lead!
    homeScoreEl.classList.remove("score-animate");
    homeScoreEl.offsetWidth;
    homeScoreEl.classList.add("score-animate");
    homeScoreEl.classList.add("score-leader");
    guestScoreEl.classList.remove("score-animate");
    guestScoreEl.classList.remove("score-leader");
    leader = "home";
  }
  if (leader !== "guest" && guestScore > homeScore) {
    // Guest takes the lead!
    homeScoreEl.classList.remove("score-animate");
    homeScoreEl.classList.remove("score-leader");
    guestScoreEl.classList.remove("score-animate");
    guestScoreEl.offsetWidth;
    guestScoreEl.classList.add("score-animate");
    guestScoreEl.classList.add("score-leader");
    leader = "guest";
  }
  if (leader !== "none" && homeScore === guestScore) {
    homeScoreEl.classList.remove("score-animate");
    guestScoreEl.classList.remove("score-animate");
    homeScoreEl.classList.remove("score-leader");
    guestScoreEl.classList.remove("score-leader");
    homeScoreEl.offsetWidth;
    guestScoreEl.offsetWidth;
    homeScoreEl.classList.add("score-animate");
    guestScoreEl.classList.add("score-animate");
    leader = "none";
  }
}

window.addHome = function addHome(amount) {
  homeScore += amount;
  homeScoreEl.textContent = homeScore;
  updateScores();
};

window.addGuest = function addGuest(amount) {
  guestScore += amount;
  guestScoreEl.textContent = guestScore;
  updateScores();
};

window.newGame = function newGame() {
  homeScore = 0;
  guestScore = 0;
  leader = "unknown";
  updateScores();
  timeLeft = 12 * 60;
  period = 1;
  stopTimer();
  updateTime();
};

newGame();
